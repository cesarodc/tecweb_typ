<?php
session_start();

if (!isset($_SESSION['admin'])) {
    echo '
    <script>
        alert("Porfavor inicie sesion");
        window.location = "index.php";
    </script>
    ';
    session_destroy();
    die();
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>GoodBunny</title>
  <!-- BOOTSTRAP 4  -->
  <link rel="stylesheet" href="https://bootswatch.com/4/pulse/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Open+Sans:wght@400;600&display=swap" rel="stylesheet">
</head>

<body>

  <!-- BARRA DE NAVEGACIÓN  -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" style="color:#ffb58a;font-weight:bold; font-size: 25px;" href="inicio_admin.html">GOODBUNNY</a>


    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto"></ul>
      <form class="form-inline my-2 my-lg-0">

        <input class="form-control mr-sm-2" name="search" id="search" type="search" placeholder="Buscar..." aria-label="Search">
        <button class="btn btn-info my-2 my-sm-0" style="padding: 7px;" type="button" onclick="location.href='series_mod.php'">BD Series</button>
        <button class="btn btn-danger my-2 my-sm-0" style="padding: 7px;" type="button" onclick="location.href='php/cerrar_sesion.php'">Cerrar sesión</button>
      </form>
    </div>
  </nav>



  <div class="container">
    <div class="row p-4">
      <div class="col-md-5">
        <div class="card">
          <div class="card-body">
            <!-- FORMULARIO PARA AGREGAR PRODUCTO -->
            <form id="product-form">
              <div class="form-group">
                <input class="form-control" type="text" id="titulo" placeholder="Titulo de la Pelicula" required>
              </div>
              <div class="form-group">
                <input class="form-control" type="text" id="duracion" placeholder="Duracion" required>
              </div>
              <div class="form-group">
                <input class="form-control" type="text" id="rutaPortada" placeholder="assets/images/example.jpg" required>
              </div>
              <div class="form-group">
                <input class="form-control" type="text" id="region" placeholder="1-8" required>
              </div>
              <div class="form-group">
                <input class="form-control" type="text" id="genero" placeholder="1-12" required>
              </div>
              <div class="form-group">
                <input class="form-control" type="text" id="clas" placeholder="3-8" required>
              </div>
              
              <input type="hidden" id="id_pelicula">


              <button class="btn btn-primary btn-block text-center" type="submit">
                Agregar Producto
              </button>
            </form>
          </div>
        </div>
      </div>

      
      <!-- TABLA  -->
      <div class="col-md-7">
        <div class="card my-4" id="product-result">
          <div class="card-body">
            <!-- RESULTADO -->
            <ul id="container"></ul>
          </div>
        </div>

        <table class="table table-bordered table-sm">
          <thead>
            <tr>
              <td>Id</td>
              <td>Titulo</td>
              <td>Descripción</td>
            </tr>
          </thead>
          <tbody id="products"></tbody>
        </table>
      </div>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

  <!-- Lógica del Frontend -->
  <script src="app.js"></script>
</body>
<hr color="white">
<footer>
  <h2 align="center" style="color:#ffb58a;font-family: 'Bebas Neue', cursive;font-weight: normal;">GoodBunny S.A de C.V
  </h2>
</footer>

</html>