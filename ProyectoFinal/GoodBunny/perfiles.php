<?php
session_start();

if (!isset($_SESSION['usuario'])) {
    echo '
    <script>
        alert("Porfavor inicie sesion");
        window.location = "index.php";
    </script>
    ';
    session_destroy();
    die();
}


?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>GoodBunny</title>
    <link rel="stylesheet" href="assets/css/style_perf.css">
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Open+Sans:wght@400;600&display=swap" rel="stylesheet">

</head>

<body style="background-color:black;">
<h2 align="left" style="color:#ffb58a;font-family: 'Bebas Neue', cursive;font-weight: normal; margin-top:25px; margin-left:25px;">GoodBunny</h2>
<hr color="white">
    <div class="container">
        <div class="row justify-content-center text-center pt-5 mt-5">
            <div class="col-md-6">
                <h2 style="color:#ffb58a;font-weight: normal;">¿Quien esta viendo ahora?</h2>
                <div class="form-group">
                    <br><br>
                </div>
            </div>
        </div>
        <div class="row justify-content-center text-center">
            <?php
            include("php/conexion_be.php");
            $con = "SELECT id_cuenta FROM usuarios WHERE usuario='$_SESSION[usuario]'";
            $cona = mysqli_query($conexion, $con);
            //print_r($cona);
            $row1 = mysqli_fetch_assoc($cona);
            $hc = "SELECT * from perfil where id_cuenta='$row1[id_cuenta]' AND eliminado=0";

            $consulta = mysqli_query($conexion, $hc);
            //echo $consulta->num_rows;

            while ($row = mysqli_fetch_assoc($consulta)) {
                if ($row["edad"] >=15) { ?>
                    <div class="col-md-2 col-sm-2 col-xl-2 col-lg-1 pb-1 text-center perfiles">
                        <button class="btn <?php echo $row["colorbg"]; ?>" onclick="location.href='principal.php?id=<?php echo $row['id_perfil']; ?>'">
                            <img src="<?php echo $row["rutaimagen"]; ?>" width="150px" alt="" srcset="">
                        </button>
                        <h4 style="color:#ffb58a;font-weight: normal;"><?php echo $row["usuario"]; ?></h4>
                        
                    </div>
                <?php } else { ?>
                    <div class="col-md-2 col-sm-2 col-xl-2 col-lg-1 pb-1 text-center perfiles">

                        <button class="btn <?php echo $row["colorbg"]; ?>" onclick="location.href='principal_niños.php?id=<?php echo $row['id_perfil']; ?>'">
                            <img src="<?php echo $row["rutaimagen"]; ?>" width="150px" alt="" srcset="">
                        </button>
                        <h4 style="color:#ffb58a;font-weight: normal;"><?php echo $row["usuario"]; ?></h4>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center text-center pt-5 mt-5">
            <div class="col-md-6 pt-4">
                <?php
                $PD = $consulta->num_rows;
                if ($PD > 0  && $PD < 7) {
                ?>
                    <div class="form-group">
                        <button class="btn btn-success btn-lg" data-toggle="modal" data-target="#exampleModal">Añadir perfil
                        </button>
                    </div>
                    <div class="form-group">
                        <h5 style="color:#ffb58a;font-weight: normal;" id="perfildis">Perfiles Disponibles:
                            <span class="text-primary" id="perdispo">
                                <?php
                                $PD = 7 - $consulta->num_rows;
                                echo $PD;
                                ?>
                            </span>
                        </h5>
                    </div>
                <?php
                } elseif($PD==0) {
                ?>
                    <h4 id="nohay" class="text-info">No hay perfiles registrados, porfavor agregue uno.</h4>
                    <div class="form-group">
                        <button class="btn btn-success btn-lg" data-toggle="modal" data-target="#exampleModal">Añadir perfil
                        </button>
                    </div>
                    <div class="form-group">
                        <h5 style="color:#ffb58a;font-weight: normal;" id="perfildis">Perfiles Disponibles:
                            <span class="text-primary" id="perdispo">
                                <?php
                                $PD = 7 - $consulta->num_rows;
                                echo $PD;
                                ?>
                            </span>
                        </h5>
                    </div>

                <?php
                }
                ?>
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Añadir Usuario</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <form action="php/add_user.php" method="POST" class="formulario__login">
                                    <div class="form-group text-left mx-sm-5">
                                        <label for="">Usuario</label>
                                        <input type="text" class="form-control" name="user">
                                    </div>
                                    <div class="form-group text-left mx-sm-5">
                                        <label for="">Idioma</label>
                                        <input type="text" class="form-control" name="idioma" id="">
                                    </div>
                                    <div class="form-group text-left mx-sm-5">
                                        <label for="">Edad </label>
                                        <input type="text" class="form-control" name="edad" id="">
                                    </div>
                                    <div class="form-group text-left mx-sm-5">
                                        <label for="">Imagen</label>
                                        <select name="imagen" id="" class="form-control">
                                            <option value="" selected> Seleccionar...</option>
                                            <option value="assets/images/p1.jpg">Imagen 1</option>
                                            <option value="assets/images/p2.jpg">Imagen 2</option>
                                            <option value="assets/images/p3.jpg">Imagen 3</option>
                                            <option value="assets/images/p4.jpg">Imagen 4</option>
                                            <option value="assets/images/p5.jpg">Imagen 5</option>
                                        </select>
                                    </div>
                                    <div class="form-group text-left mx-sm-5">
                                        <label for="">Color</label>
                                        <select name="colorbg" id="" class="form-control">
                                            <option value="" selected> Seleccionar...</option>
                                            <option class="bg-danger text-white" value="bg-danger">DANGER</option>
                                            <option class="bg-primary text-white" value="bg-primary">PRIMARY</option>
                                            <option class="bg-warning text-white" value="bg-danger">WARNING</option>
                                            <option class="bg-info text-white" value="bg-info">INFO</option>
                                            <option class="bg-dark text-white" value="bg-dark">DARK</option>
                                        </select>
                                    </div>
                                    <button class="btn btn-primary">Crear Perfil</button>
                                </form>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal" onclick="location.href='mod_perfiles.php?nombre=Pepe'">Editar Perfiles</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="location.href='php/cerrar_sesion.php'">Cerrar sesión</button>            
        </div>
        
            
        
    </div>


    
		
	




    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous">
    </script>
</body>

</html>