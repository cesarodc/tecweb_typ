<?php
session_start();

if (!isset($_SESSION['usuario'])) {
    echo '
    <script>
        alert("Porfavor inicie sesion");
        window.location = "index.php";
    </script>
    ';
    session_destroy();
    die();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>GoodBunny</title>
    <!-- BOOTSTRAP 4  -->
    <link rel="stylesheet" href="https://bootswatch.com/4/pulse/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Open+Sans:wght@400;600&display=swap" rel="stylesheet">
</head>

<body>
    <!-- BARRA DE NAVEGACIÓN  -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" style="color:#ffb58a;font-weight:bold; font-size: 25px;" href="inicio_admin.html">GOODBUNNY</a>


        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto"></ul>
            <form class="form-inline my-2 my-lg-0">
                <button style="padding: 7px 20px;border: 2px solid #ffb58a;font-size: 12px;background: transparent;font-weight: 600;cursor: pointer;color: white;outline: none;transition: all 300ms;" type="button" onclick="location.href='php/cerrar_sesion.php'">Cerrar sesión</button>
                <button style="padding: 7px 18px;border: 2px solid #ffb58a;font-size: 12px;background: transparent;font-weight: 600;cursor: pointer;color: white;outline: none;transition: all 300ms;" type="button" onclick="location.href='perfiles.php'">Regresar</button>
            </form>
        </div>
    </nav>



    <div class="container">
        <div class="row p-4">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-body">
                        <!-- FORMULARIO PARA AGREGAR PRODUCTO -->
                        <form id="product-form">
                            <div class="form-group">
                                <input class="form-control" id="nombre" type="text" style="margin-top: 10px; padding: 8px; border: none; background: #DEDEDE; font-size: 16px; outline: none;" placeholder="Nombre de usuario" required>
                            </div>
                            <div class="form-group">
                            <input class="form-control" id="idioma" type="text"  style="margin-top: 10px; padding: 8px; border: none; background: #DEDEDE; font-size: 16px; outline: none;" placeholder="Idioma" required>
                            </div>
                            <div class="form-group">
                            <input class="form-control" id="edad" type="text"  style="margin-top: 10px; padding: 8px; border: none; background: #DEDEDE; font-size: 16px; outline: none;" placeholder="Edad" required>
                            </div>
                            <div class="form-group">
                            <input class="form-control" id="rutaImagen" type="text"  style="margin-top: 10px; padding: 8px; border: none; background: #DEDEDE; font-size: 16px; outline: none;" placeholder="assets/images/p1.png" required>
                            </div>
                            <div class="form-group">
                            <input class="form-control" id="colorrgb" type="text"  style="margin-top: 10px; padding: 8px; border: none; background: #DEDEDE; font-size: 16px; outline: none;" placeholder="bg-danger" required>
                            </div>
                            

                            <input type="hidden" id="id_perfil">
                            <?php
                            include("php/conexion_be.php");
                            $con = "SELECT id_cuenta FROM usuarios WHERE usuario='$_SESSION[usuario]' AND eliminado=0";
                            $cona = mysqli_query($conexion, $con);
                            //print_r($cona);
                            $row = mysqli_fetch_assoc($cona);
                            //echo $row["id_cuenta"];
                            ?>
                            <input type="hidden" id="id_cuenta" value="<?php echo $row["id_cuenta"]; ?>">
                            <button class="btn btn-primary btn-block text-center" type="submit">
                                Confirmar Edicion
                            </button>
                        </form>
                    </div>
                </div>
            </div>


            <!-- TABLA  -->
            <div class="col-md-7">
                <div class="card my-4" id="product-result">
                    <div class="card-body">
                        <!-- RESULTADO -->
                        <ul id="container"></ul>
                    </div>
                </div>

                <table id="table" cellspacing="25px">
                    <h3 style="color:white;font-family: 'Bebas Neue', cursive; font-weight: normal;font-size:28px; margin-left:20%">Perfiles</h3>
                    <thead>
                        <tr>
                            <td align="center" style="color:#ffb58a;">Imagen</td>
                            <td align="center" style="color:#ffb58a;">Nombre</td>
                            <td align="center" style="color:#ffb58a;">Descripción</td>
                        </tr>
                    </thead>
                    <tbody id="products"></tbody>
                </table>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

    <!-- Lógica del Frontend -->
    <script src="appPer.js"></script>
</body>
<hr color="white">
<footer>
    <h2 align="center" style="color:#ffb58a;font-family: 'Bebas Neue', cursive;font-weight: normal;">GoodBunny S.A de C.V
    </h2>
</footer>

</html>