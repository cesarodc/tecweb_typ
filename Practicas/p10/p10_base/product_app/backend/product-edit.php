<?php
    use Tecwebns\Update\Update as Update;
    require_once __DIR__.'/api/start.php';

    $productos = new Update();
    $productos->edit( json_decode( json_encode($_POST) ) );
    echo $productos->getResponse();
?>