<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(__DIR__);
$baseDir = dirname($vendorDir);

return array(
    'Composer\\InstalledVersions' => $vendorDir . '/composer/InstalledVersions.php',
    'Tecwebns\\Create\\Create' => $baseDir . '/backend/api/Tecwebns/Create/Create.php',
    'Tecwebns\\DataBase' => $baseDir . '/backend/api/Tecwebns/DataBase.php',
    'Tecwebns\\Delete\\Delete' => $baseDir . '/backend/api/Tecwebns/Delete/Delete.php',
    'Tecwebns\\Read\\Read' => $baseDir . '/backend/api/Tecwebns/Read/Read.php',
    'Tecwebns\\Update\\Update' => $baseDir . '/backend/api/Tecwebns/Update/Update.php',
);
