<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<?php
	

	
		/** SE CREA EL OBJETO DE CONEXION */
		@$link = new mysqli('localhost', 'root', 'Paraiso12#', 'marketzone');	

		/** comprobar la conexión */
	
	?>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Producto</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script>
            function show() {
				
                // se obtiene el id de la fila donde está el botón presinado
                var rowId = event.target.parentNode.parentNode.id;
				alert(rowId);
                // se obtienen los datos de la fila en forma de arreglo
                var data = document.getElementById(rowId).querySelectorAll(".row-data");
                /**
                querySelectorAll() devuelve una lista de elementos (NodeList) que 
                coinciden con el grupo de selectores CSS indicados.
                (ver: https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors)

                En este caso se obtienen todos los datos de la fila con el id encontrado
                y que pertenecen a la clase "row-data".
                */
				
                var nombre = data[0].innerHTML;
                var marca = data[1].innerHTML;
				var modelo = data[2].innerHTML;
				var precio = data[3].innerHTML;
				var unidades = data[4].innerHTML;
				var detalles = data[5].innerHTML;
				var imagen = data[6].innerHTML;
				
				
                alert("Nombre: " + nombre + "\nMarca: " + marca + "\nModelo: "+ modelo+ "\nPrecio: "+precio+"\nUnidades: "+unidades+"\nDetalles: "+detalles+"\nImagen: "+imagen);

                send2form(nombre, marca,modelo,precio,unidades,detalles,imagen);
            }
        </script>

	</head>
	<body>
		<h3>PRODUCTO</h3>

		<?php 
                    
                    $pod = "SELECT * from productos WHERE eliminado = 0";

                    $consulta = mysqli_query($link,$pod);
                    while($row=mysqli_fetch_assoc($consulta)) { ?>
                      
                      <table class="table">
				<thead class="thead-dark">
					<tr>
					<th scope="col">#</th>
					<th scope="col">Nombre</th>
					<th scope="col">Marca</th>
					<th scope="col">Modelo</th>
					<th scope="col">Precio</th>
					<th scope="col">Unidades</th>
					<th scope="col">Detalles</th>
					<th scope="col">Imagen</th>
					<th scope="col">Modificar</th>
					</tr>
				</thead>
				<tbody>
					<tr id="1">
					<th scope="row"><?= $row['id'] ?></th>
						<td class="row-data"><?= $row['nombre'] ?></td>
						<td class="row-data"><?= $row['marca'] ?></td>
						<td class="row-data"><?= $row['modelo'] ?></td>
						<td class="row-data"><?= $row['precio'] ?></td>
						<td class="row-data"><?= $row['unidades'] ?></td>
						<td class="row-data"><?= utf8_encode($row['detalles']) ?></td>
						<td class="row-data"><img src=<?= $row['imagen'] ?> ></td>
						<td><input type="button" 
                               value="Actualizar" 
                               onclick="show()" /></td>
					</tr>
				</tbody>
			</table>

                    <?php }  ?>  

		<script>
            function send2form(nombre, marca,modelo,precio,unidades,detalles,imagen){     //form) { 
                var urlForm = "http://localhost/p07-base/formulario_productos_v2.php";
                var propNombre = "nombre="+nombre;
                var propMarca = "marca="+marca;
				var propModelo = 'modelo='+modelo;
				var propPrecio = 'precio='+precio;
				var propUnidades = 'unidades='+unidades;
				var propDetalles='detalles='+detalles;
				var propImagen='imagen='+imagen;
                window.open(urlForm+"?"+propNombre+"&"+propMarca+"&"+propModelo+"&"+propPrecio+"&"+propUnidades+"&"+propDetalles+"&"+propImagen);
            }
        </script>
	
	</body>
</html>