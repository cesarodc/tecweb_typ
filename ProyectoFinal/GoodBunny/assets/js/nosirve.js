

function buscar_datos(consulta){
    $.ajax({
        url: 'principal.php',
        type: 'POST',
        dataType:'html',
        data:{consulta:consulta},
    })
    .done(function(respuesta){
        $("#datos").html(respuesta);
    })
    .fail(function(){
        console.log("error");
    })
    
}

$(buscar_datos());

$(document).on('keyup', '#search',function(){
    var valor = $(this).val();
    if(valor != ""){
        buscar_datos(valor);
    }else{
        buscar_datos();
    }

} );
















$(document).ready(function () {


    // Testing Jquery
    console.log('Search is working!');
   
    $('#product-result').hide();
    $('#table').hide();
  
    // search key type event
    $('#search').keyup(function () {
      if ($('#search').val()) {
        let search = $('#search').val();
        console.log('buscando...' + search);
        $.ajax({
          url: 'backend/product-search.php',
          data: { search },
          type: 'GET',
          success: function (response) {
            console.log(response);
            if (!response.error) {
              let products = JSON.parse(response);
              let template1 = '';
              let template = '';
              products.forEach(product => {
                template1 += `
                      <li><a href="#" class="product-item">${product.titulo}</a></li>
                       `
  
                let description = '';
                description += '<li>Duracion: ' + product.duracion + '</li>';
                description += '<li>ID Region: ' + product.id_region + '</li>';
                description += '<li>ID Genero: ' + product.id_genero + '</li>';
                description += '<li>ID Clasificacion: ' + product.id_clas + '</li>';
  
                template += `
                      <tr>
                      <td><img width='80%' height='160' src="${product.rutaPortada}"' alt=''></td>
                      <td>
                      <a href="peliculas/${product.titulo}.php" class="product-item">
                        ${product.titulo} 
                      </a>
                      </td>
                      <td>${description}</td>
                      </tr>
                    `
              });
              //console.log(response);
              $('#product-result').show();
              $('#table').show();
              //$('#container').html(template1);
              //console.log(template);
              $('#products').html(template);
            }
          }
        })
      }
      else if($('#search').val()==""){
        $('#product-result').show();
        $('#table').show();
      }
    });
   
  });
  