<?php
    include_once __DIR__.'/database.php';
    // SE OBTIENE LA INFORMACIÓN DEL PRODUCTO ENVIADA POR EL CLIENTE
    $data = array(
        'status'  => 'error',
        'message' => 'Ya existe una pelicula con ese nombre'
    );
    if(isset($_POST['titulo'])) {
        // SE TRANSFORMA EL POST A UN STRING EN JSON, Y LUEGO A OBJETO
        $jsonOBJ = json_decode( json_encode($_POST) );
        // SE ASUME QUE LOS DATOS YA FUERON VALIDADOS ANTES DE ENVIARSE
        $sql = "SELECT * FROM serie WHERE titulo='{$jsonOBJ->titulo}' AND eliminado = 0";
	    $result = $conexion->query($sql);
        
        if ($result->num_rows == 0) {
            $conexion->set_charset("utf8");
            $sql = "INSERT INTO serie VALUES (null, '{$jsonOBJ->titulo}', '{$jsonOBJ->numTemp}','{$jsonOBJ->totalCap}', '{$jsonOBJ->rutaPortada}', {$jsonOBJ->id_region}, '{$jsonOBJ->id_genero}', {$jsonOBJ->id_clas},0)";
            if($conexion->query($sql)){
                $data['status'] =  "success";
                $data['message'] =  "Pelicula agregada";
            } else {
                $data['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($conexion);
            }
        }

        $result->free();
        // Cierra la conexion
        $conexion->close();
    }

    // SE HACE LA CONVERSIÓN DE ARRAY A JSON
    echo json_encode($data, JSON_PRETTY_PRINT);
?>