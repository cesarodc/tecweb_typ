<?php
    require_once __DIR__ . '/API/Productos.php';
    $add = new Productos('marketzone');

    $producto = file_get_contents('php://input');
    if(!empty($producto)) {
        // SE TRANSFORMA EL POST A UN STRING EN JSON, Y LUEGO A OBJETO
        
        $jsonOBJ = json_decode( json_encode($_POST) );
        $add->add($jsonOBJ);
    }
    $addS = $add->getResponse();

?>