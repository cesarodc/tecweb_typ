$(document).ready(function () {
  // Global Settings
  let edit = false;

  // Testing Jquery
  console.log('jquery is working!');
  fetchproducts();
  $('#product-result').hide();


  // search key type event
  $('#search').keyup(function () {
    if ($('#search').val()) {
      let search = $('#search').val();
      console.log('buscando...' + search);
      $.ajax({
        url: 'backend/product-search.php',
        data: { search },
        type: 'GET',
        success: function (response) {
          console.log(response);
          if (!response.error) {
            let products = JSON.parse(response);
            let template1 = '';
            let template = '';
            products.forEach(product => {
              template1 += `
                    <li><a href="#" class="product-item">${product.titulo}</a></li>
                     `

              let description = '';
              description += '<li>Duracion: ' + product.duracion + '</li>';
              description += '<li>Ruta de portada: ' + product.rutaPortada + '</li>';
              description += '<li>ID Region: ' + product.id_region + '</li>';
              description += '<li>ID Genero: ' + product.id_genero + '</li>';
              description += '<li>ID Clasificacion: ' + product.id_clas + '</li>';

              template += `
                    <tr productId="${product.id_peliculas}">
                    <td>${product.id_peliculas}</td>
                    <td>
                    <a href="#" class="product-item">
                      ${product.titulo} 
                    </a>
                    </td>
                    <td>${description}</td>
                    <td>
                      <button class="product-delete btn btn-danger">
                       Delete 
                      </button>
                    </td>
                    </tr>
                  `
            });
            //console.log(response);
            $('#product-result').show();
            $('#container').html(template1);
            //console.log(template);
            $('#products').html(template);
          }
        }
      })
    }
  });
  /////////////////////////// Agregar Productos
  $('#product-form').submit(e => {
    e.preventDefault();
    
    //('select[name=region]').change(function(){ region = $(this).val(); console.log(region);})

    let postData = {
      titulo: $('#titulo').val(),
      duracion: $('#duracion').val(),
      rutaPortada: $('#rutaPortada').val(),
      id_region:$('#region').val(),
      id_genero:$('#genero').val(),
      id_clas:$('#clas').val(),
      id: $('#id_pelicula').val()

    }

    //console.log(postData);
    const url = edit === false ? './backend/product-add.php' : './backend/product-edit.php';

    $.post(url, postData, (response) => {
      console.log(response); 
      // SE REINICIA EL FORMULARIO
      $('#titulo').val('');
      $('#duracion').val('');
      $('#rutaPortada').val('');
      $('#region').val('');
      $('#genero').val('');
      $('#class').val('');

      // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
      let respuesta = JSON.parse(response);
      // SE CREA UNA PLANTILLA PARA CREAR INFORMACIÓN DE LA BARRA DE ESTADO
      let template_bar = '';
      template_bar += `
                          <li style="list-style: none;">status: ${respuesta.status}</li>
                          <li style="list-style: none;">message: ${respuesta.message}</li>
                      `;

      // SE HACE VISIBLE LA BARRA DE ESTADO
      $('#product-result').show();
      // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
      $('#container').html(template_bar);
      // SE LISTAN TODOS LOS PRODUCTOS
      fetchproducts();
      // SE REGRESA LA BANDERA DE EDICIÓN A false
      edit = false;
    });

  });
  /////////////////////////////// Fin agregar
  // Fetching products
  function fetchproducts() {
    $.ajax({
      url: 'backend/product-list.php',
      type: 'GET',
      success: function (response) {
        console.log("Lista:" + response);
        const products = JSON.parse(response);
        let template = '';
        products.forEach(product => {
          let description = '';
          description += '<li>Duracion: ' + product.duracion + '</li>';
          description += '<li>Ruta de portada: ' + product.rutaPortada + '</li>';
          description += '<li>ID Region: ' + product.id_region + '</li>';
          description += '<li>ID Genero: ' + product.id_genero + '</li>';
          description += '<li>ID Clasificacion: ' + product.id_clas + '</li>';

          template += `
                    <tr productId="${product.id_peliculas}">
                    <td>${product.id_peliculas}</td>
                    <td>
                    <a href="#" class="product-item">
                      ${product.titulo} 
                    </a>
                    </td>
                    <td>${description}</td>
                    <td>
                      <button class="product-delete btn btn-danger">
                       Delete 
                      </button>
                    </td>
                    </tr>
                  `
        });
        $('#products').html(template);
      }
    });
  }

  // Get a Single product by Id 
  $(document).on('click', '.product-item', (e) => {
    const element = $(this)[0].activeElement.parentElement.parentElement;
    //console.log(element);
    const id = $(element).attr('productId');
    //console.log("ID: "+id);
    $.post('./backend/product-single.php', {id}, (response) => {
        //console.log("respuesta "+ response);
        // SE CONVIERTE A OBJETO EL JSON OBTENIDO
        let product = JSON.parse(response);
        // SE INSERTAN LOS DATOS ESPECIALES EN LOS CAMPOS CORRESPONDIENTES
        $('#titulo').val(product.titulo);
        $('#duracion').val(product.duracion);
        $('#rutaPortada').val(product.rutaPortada);
        $('#region').val(product.id_region);
        $('#genero').val(product.id_genero);
        $('#clas').val(product.id_clas);
        
        // EL ID SE INSERTA EN UN CAMPO OCULTO PARA USARLO DESPUÉS PARA LA ACTUALIZACIÓN
        $('#id_pelicula').val(product.id_peliculas);
        // SE ELIMINA nombre, eliminado E id PARA PODER MOSTRAR EL JSON EN EL <textarea>
        //delete(product.nombre);
        //delete(product.eliminado);
        //delete(product.id);
        // SE CONVIERTE EL OBJETO JSON EN STRING
        //let JsonString = JSON.stringify(product,null,2);
        // SE MUESTRA STRING EN EL <textarea>

        //$('#description').val(JsonString);
        
        // SE PONE LA BANDERA DE EDICIÓN EN true
        edit = true;
    });
    e.preventDefault();
}); 

  // Delete a Single product
  $(document).on('click', '.product-delete', (e) => {
    if (confirm('¿ESTAS SEGURO DE ELIMIAR ESTE PRODUCTO?')) {
      const element = $(this)[0].activeElement.parentElement.parentElement;
      const id = $(element).attr('productId');
      $.post('backend/product-delete.php', { id }, (response) => {
        console.log('Elemento eliminado')
        fetchproducts();
      });
    }
  });
});


