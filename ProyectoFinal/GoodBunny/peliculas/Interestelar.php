<?php
session_start();

if (!isset($_SESSION['usuario'])) {
	echo '
    <script>
        alert("Porfavor inicie sesion");
        window.location = "index.php";
    </script>
    ';
	session_destroy();
	die();
}

include '../php/conexion_be.php';

$hc = "SELECT * from peliculas where id_peliculas='1'";
$consulta = mysqli_query($conexion, $hc);
$row = mysqli_fetch_assoc($consulta);

$hc1 = "SELECT * from clasificacion where id_clas='$row[id_clas]'";
$consulta1 = mysqli_query($conexion, $hc1);
$row2 = mysqli_fetch_assoc($consulta1);

$hc2 = "SELECT * from genero where id_genero='$row[id_genero]'";
$consulta2 = mysqli_query($conexion, $hc2);
$row3 = mysqli_fetch_assoc($consulta2);

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="../assets/css/estilosPrin.css">
	<link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Open+Sans:wght@400;600&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat&family=Open+Sans&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.1/normalize.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/glider-js@1.7.3/glider.min.css">
	<style>
		.dropdown {
			position: relative;
			display: inline-block;
		}

		.dropdown-content {
			display: none;
			position: absolute;
			background-color: black;
			min-width: 230px;
			box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
			padding: 18px 20px;
			z-index: 1;
		}

		.dropdown:hover .dropdown-content {
			display: block;
		}
	</style>
	<title>Interestelar</title>
</head>

<body>
	<header>
		<div class="contenedor">
			<h2 class="logotipo">GoodBunny</h2>
			<nav>
				<a href="">Inicio</a>
			</nav>
		</div>
	</header>

	<div id="result">
		<div class="indicadores"></div>
		<table id="table" cellspacing="15px">
			<h3 style="color:white;font-family: 'Bebas Neue', cursive; font-weight: normal;font-size:28px; margin-left:20%">Películas Encontradas</h3>
			<thead>
				<tr>
					<td></td>
					<td align="center" style="color:#ffb58a;">Titulo</td>
					<td align="center" style="color:#ffb58a;">Descripción</td>
					<td></td>
				</tr>
			</thead>
			<tbody id="content"></tbody>
		</table>
	</div>
	</div>

	<div id="result1">
		<div class="indicadores"></div>
		<table id="table1" cellspacing="15px">
			<h3 style="color:white; font-family: 'Bebas Neue', cursive;font-weight: normal; font-size:28px; margin-left:20%">Series Encontradas</h3>
			<thead>
				<tr>
					<td></td>
					<td align="center" style="color:#ffb58a;">Titulo</td>
					<td align="center" style="color:#ffb58a;">Descripción</td>
					<td></td>
				</tr>
			</thead>
			<tbody id="content1"></tbody>
		</table>
	</div>
	</div>


	<main>
		<div class="pelicula-principal">
			<div class="contenedor">
				<h3 class="titulo">Interestellar</h3>
				<p class="descripcion">
					Narra las aventuras de un grupo de exploradores que hacen uso de un agujero de gusano recientemente descubierto para superar las limitaciones de los viajes espaciales tripulados y vencer las inmensas distancias que tiene un viaje interestelar.
				</p>
				<div> Duracion: <?php echo $row["duracion"]; ?>  &nbsp;&nbsp; Clasificacion: <?php echo $row2["clave"] ?>   &nbsp;&nbsp;Genero: <?php echo $row3["nombre"] ?></div>
			</div>
		</div>
	</main>

	<div align="center">
		<iframe width="70%" height="800px" src="https://www.youtube.com/embed/1TfbWbTBSHo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	</div>


	<script src="https://kit.fontawesome.com/2c36e9b7b1.js" crossorigin="anonymous"></script>
	<script src="../assets/js/main.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/glider-js@1.7.3/glider.min.js"></script>
	<script src="https://kit.fontawesome.com/2c36e9b7b1.js" crossorigin="anonymous"></script>
	<script src="../assets/js/app_car.js"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

	<!-- Lógica del Frontend -->
	<script src="../search.js"></script>
</body>

</html>