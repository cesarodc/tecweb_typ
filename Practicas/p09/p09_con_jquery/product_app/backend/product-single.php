<?php

include('database.php');

if(isset($_POST['id'])) {
  $id = mysqli_real_escape_string($conexion, $_POST['id']);

  $query = "SELECT * from productos WHERE id = {$id}";

  $result = mysqli_query($conexion, $query);
  if(!$result) {
    die('Query Failed'. mysqli_error($conexion));
  }

  $json = array();
  while($row = mysqli_fetch_array($result)) {
    $json[] = array(
      'name' => $row['nombre'],
      'precio' => $row['precio'],
      'unidades' => $row['unidades'],
      'modelo' => $row['modelo'],    
      'marca' => $row['marca'],  
      'detalles' => $row['detalles'],
      'imagen' => $row['imagen'],
      'id' => $row['id']

    );
  }
  
  $jsonstring = json_encode($json[0]);
  echo $jsonstring;
}

?>