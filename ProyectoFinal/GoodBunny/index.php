<?php
 session_start();
  if(isset($_SESSION['admin'])){
      header("location:inicio_admin.php");
  }

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GoodBunny</title>
    
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">


    <link rel="stylesheet" href="assets/css/estilos.css">
</head>
<body>

        <main>

            <div class="contenedor__todo">
                <div class="caja__trasera">
                    <div class="caja__trasera-login">
                        <h3>¿Ya tienes una cuenta?</h3>
                        <p>Inicia sesión para entrar en la página</p>
                        <button id="btn__iniciar-sesion">Iniciar Sesión</button>
                    </div>
                    <div class="caja__trasera-register">
                        <h3>¿Aún no tienes una cuenta?</h3>
                        <p>Regístrate para que puedas iniciar sesión</p>
                        <button id="btn__registrarse">Regístrarse</button>
                    </div>
                </div>

                <!--Formulario de Login y registro-->
                <div class="contenedor__login-register">
                    <!--Login-->
                    <form action="php/login_usuario_be.php" method="POST" class="formulario__login">
                        <h2>Iniciar Sesión</h2>
                        <input type="text" placeholder="Usuario" name="correo">
                        <input type="password" placeholder="Contraseña" name="contrasena">
                        <button>Entrar</button>
                    </form>

                    <!--Register-->
                    <form action="php/registro_usuario_be.php" method="POST" class="formulario__register">
                        <h2>Regístrarse</h2>
                        <input type="text" placeholder="Nombre" name="nombre" required>
                        <input type="text" placeholder="Apellidos" name="apellidos" required>
                        <select name="tipo" style="width: 100%; margin-top: 20px; padding: 10px; border: none; background: #494949; font-size: 16px; outline: none;"> 
                                        <option value="" selected> Seleccionar Tipo</option>
                                        <option value="Free">Free</option>
                                        <option value="Familiar">Familiar</option>
                                        <option value="Premium">Premium</option>                        
                        </select>
                        <input type="text" placeholder="Pais" name="pais" required>
                        <input type="text" placeholder="Numero de Tarjeta" name="tarjeta" required>
                        <select name="peridiocidad" style="width: 100%; margin-top: 20px; padding: 10px; border: none; background: #494949; font-size: 16px; outline: none;">
                                        <option value="" selected> Seleccionar Peridiocidad</option>
                                        <option value="Free">Anual</option>
                                        <option value="Familiar">Mensual</option>                                        
                        </select>   
                        <input type="text" placeholder="Usuario" name="usuario" required>
                        <input type="password" placeholder="Contraseña" name="contrasena" required>

                        <button>Regístrarse</button>
                    </form>
                </div>
            </div>

        </main>

        <script src="assets/js/script.js"></script>
</body>
</html>