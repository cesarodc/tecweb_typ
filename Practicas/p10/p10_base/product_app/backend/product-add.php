<?php
    use Tecwebns\Create\Create as Create;
    require_once __DIR__.'/api/start.php';

    $productos = new Create();
    $productos->add( json_decode( json_encode($_POST) ) );
    echo $productos->getResponse();
?>