// JSON BASE A MOSTRAR EN FORMULARIO
var baseJSON = {
    "precio": 0.0,
    "unidades": 1,
    "modelo": "XX-000",
    "marca": "NA",
    "detalles": "NA",
    "imagen": "img/default.png"
  };

function init() {
    /**
     * Convierte el JSON a string para poder mostrarlo
     * ver: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON
     */
    var JsonString = JSON.stringify(baseJSON,null,2);
    document.getElementById("description").value = JsonString;

    // SE LISTAN TODOS LOS PRODUCTOS
    //listarProductos();
}

$(document).ready(function() {
    // Global Settings
    let edit = false;
  
    // Testing Jquery
    console.log('jquery is working!');
    fetchproducts();
    $('#product-result').hide();
  
  
    // search key type event
    $('#search').keyup(function() {
      if($('#search').val()) {
        let search = $('#search').val();
        //console.log('buscando...' + search);
        $.ajax({
          url: 'backend/product-search.php',
          data: {search},
          type: 'GET',
          success: function (response) {
            if(!response.error) {
              let products = JSON.parse(response);
              let template1 = '';
              let template = '';
              products.forEach(product => {
                  template1 += `
                    <li><a href="#" class="product-item">${product.nombre}</a></li>
                     ` 

                     let description = '';
                     description += '<li>precio: '+product.precio+'</li>';
                     description += '<li>unidades: '+product.unidades+'</li>';
                     description += '<li>modelo: '+product.modelo+'</li>';
                     description += '<li>marca: '+product.marca+'</li>';
                     description += '<li>detalles: '+product.detalles+'</li>';
         
                     template += `
                             <tr productId="${product.id}">
                             <td>${product.id}</td>
                             <td>
                             <a href="#" class="product-item">
                               ${product.nombre} 
                             </a>
                             </td>
                             <td>${description}</td>
                             <td>
                               <button class="product-delete btn btn-danger">
                                Delete 
                               </button>
                             </td>
                             </tr>
                           ` 
          });
          //console.log(response);
              $('#product-result').show();
              $('#container').html(template1);
              //console.log(template);
              $('#products').html(template);
            }
          } 
        })
      }
    });
  
    $('#product-form').submit(e => {
      e.preventDefault();
      var productoJsonString = document.getElementById('description').value;
    // SE CONVIERTE EL JSON DE STRING A OBJETO
    var finalJSON = JSON.parse(productoJsonString);
    // SE AGREGA AL JSON EL NOMBRE DEL PRODUCTO
    finalJSON['nombre'] = document.getElementById('name').value;
      productoJsonString = JSON.stringify(finalJSON,null,2);
      const url = edit === false ? 'backend/product-add.php' : 'backend/product-edit.php';
      //url = 'backend/product-add.php';
      console.log(productoJsonString, url);
      $.post(url, productoJsonString, (response) => {
        console.log(response);
        $('#product-form').trigger('reset');
        fetchproducts();
      });
    });
  
    // Fetching products
    function fetchproducts() {
      $.ajax({
        url: 'backend/product-list.php',
        type: 'GET',
        success: function(response) {
          const products = JSON.parse(response);
          let template = '';
          products.forEach(product => {
            let description = '';
            description += '<li>precio: '+product.precio+'</li>';
            description += '<li>unidades: '+product.unidades+'</li>';
            description += '<li>modelo: '+product.modelo+'</li>';
            description += '<li>marca: '+product.marca+'</li>';
            description += '<li>detalles: '+product.detalles+'</li>';

            template += `
                    <tr productId="${product.id}">
                    <td>${product.id}</td>
                    <td>
                    <a href="#" class="product-item">
                      ${product.nombre} 
                    </a>
                    </td>
                    <td>${description}</td>
                    <td>
                      <button class="product-delete btn btn-danger">
                       Delete 
                      </button>
                    </td>
                    </tr>
                  `
          });
          $('#products').html(template);
        }
      });
    }
  
    // Get a Single product by Id 
    $(document).on('click', '.product-item', (e) => {
      const element = $(this)[0].activeElement.parentElement.parentElement;
      const id = $(element).attr('productId');
      //console.log(id);
      $.post('backend/product-single.php', {id}, (response) => {
          //console.log(response);
        const product = JSON.parse(response);
        $('#name').val(product.name);
        //console.log(product.precio);
        var description = {
            "id": product.id,
            "precio": product.precio,
            "unidades":product.unidades,
            "modelo": product.modelo,
            "marca": product.marca,
            "detalles": product.detalles,
            "imagen": product.imagen
          };
          var JsonString = JSON.stringify(description,null,2);
        $('#description').val(JsonString);
        $('#producId').val(product.id);
        edit = true;
        
      });
      e.preventDefault();
    });
  
    // Delete a Single product
    $(document).on('click', '.product-delete', (e) => {
      if(confirm('¿ESTAS SEGURO DE ELIMIAR ESTE PRODUCTO?')) {
        const element = $(this)[0].activeElement.parentElement.parentElement;
        const id = $(element).attr('productId');
        $.post('backend/product-delete.php', {id}, (response) => {
            console.log('Elemento eliminado')
          fetchproducts();
        });
      }
    });
  });


