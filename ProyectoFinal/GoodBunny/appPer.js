$(document).ready(function () {
  // Global Settings
  let edit = false;

  // Testing Jquery
  console.log('jquery is working!');
  fetchproducts();
  $('#product-result').hide();



  /////////////////////////// Agregar Productos
  $('#product-form').submit(e => {
    e.preventDefault();
    
    //('select[name=region]').change(function(){ region = $(this).val(); console.log(region);})

    let postData = {
      usuario: $('#nombre').val(),
      idioma: $('#idioma').val(),
      edad: $('#edad').val(),
      rutaimagen: $('#rutaImagen').val(),
      colorrgb:$('#colorrgb').val(),
      id: $('#id_perfil').val()

    }
    

    console.log(postData);
    const url = edit === false ? './backend/product-addU.php' : './backend/product-editU.php';

    $.post(url, postData, (response) => {
      console.log(response); 
      // SE REINICIA EL FORMULARIO
      $('#nombre').val('');
      $('#idioma').val('');
      $('#edad').val('');
      $('#rutaImagen').val('');
      $('#colorrgb').val('');


      // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
      let respuesta = JSON.parse(response);
      // SE CREA UNA PLANTILLA PARA CREAR INFORMACIÓN DE LA BARRA DE ESTADO
      let template_bar = '';
      template_bar += `
                          <li style="list-style: none;">status: ${respuesta.status}</li>
                          <li style="list-style: none;">message: ${respuesta.message}</li>
                      `;

      // SE HACE VISIBLE LA BARRA DE ESTADO
      $('#product-result').show();
      // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
      $('#container').html(template_bar);
      // SE LISTAN TODOS LOS PRODUCTOS
      fetchproducts();
      // SE REGRESA LA BANDERA DE EDICIÓN A false
      edit = false;
    });

  });
  /////////////////////////////// Fin agregar
  // Fetching products
  function fetchproducts() {
    let id_c = $('#id_cuenta').val();
    $.ajax({
      url: 'backend/product-listU.php',
      data: { id_c},
      type: 'GET',
      success: function (response) {
        console.log("Lista:" + response);
        const products = JSON.parse(response);
        let template = '';
        products.forEach(product => {
          let description = '';
          description += '<li>idioma: ' + product.idioma + '</li>';
          description += '<li>edad: ' + product.edad + '</li>';
          description += '<li>Color BG: ' + product.colorbg + '</li>';

          template += `
                    <tr productId="${product.id_perfil}">
                    
                    <td>
                    <img width='80%' height='90' src="${product.rutaimagen}"' alt=''></td>
                    <td>
                    <a href="#" class="product-item">
                      ${product.usuario} 
                    </a>
                    </td>
                    <td>${description}</td>
                    <td>
                      <button class="product-delete btn btn-danger">
                       Delete 
                      </button>
                    </td>
                    </tr>
                  `
        });
        $('#products').html(template);
      }
    });
  }

  // Get a Single product by Id 
  $(document).on('click', '.product-item', (e) => {
    const element = $(this)[0].activeElement.parentElement.parentElement;
    //console.log(element);
    const id = $(element).attr('productId');
    //console.log("ID: "+id);
    $.post('./backend/product-singleU.php', {id}, (response) => {
        console.log("respuesta Single "+ response);
        // SE CONVIERTE A OBJETO EL JSON OBTENIDO
        let product = JSON.parse(response);
        // SE INSERTAN LOS DATOS ESPECIALES EN LOS CAMPOS CORRESPONDIENTES
        $('#nombre').val(product.usuario);
        $('#idioma').val(product.idioma);
        $('#edad').val(product.edad);
        $('#rutaImagen').val(product.rutaimagen);
        $('#colorrgb').val(product.colorbg);
        
        
        // EL ID SE INSERTA EN UN CAMPO OCULTO PARA USARLO DESPUÉS PARA LA ACTUALIZACIÓN
        $('#id_perfil').val(product.id_perfil);
        // SE ELIMINA nombre, eliminado E id PARA PODER MOSTRAR EL JSON EN EL <textarea>
        //delete(product.nombre);
        //delete(product.eliminado);
        //delete(product.id);
        // SE CONVIERTE EL OBJETO JSON EN STRING
        //let JsonString = JSON.stringify(product,null,2);
        // SE MUESTRA STRING EN EL <textarea>

        //$('#description').val(JsonString);
        
        // SE PONE LA BANDERA DE EDICIÓN EN true
        edit = true;
    });
    e.preventDefault();
}); 

  // Delete a Single product
  $(document).on('click', '.product-delete', (e) => {
    if (confirm('¿ESTAS SEGURO DE ELIMIAR ESTE PRODUCTO?')) {
      const element = $(this)[0].activeElement.parentElement.parentElement;
      const id = $(element).attr('productId');
      $.post('backend/product-deleteU.php', { id }, (response) => {
        console.log('Elemento eliminado')
        fetchproducts();
      });
    }
  });
});


