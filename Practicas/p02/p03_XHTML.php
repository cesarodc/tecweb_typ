<?php
echo " P1 ---- Variables validas --------------------------------------------------------------";
$_myvar = "Valida";
$_7var =  "Valida";
//myvar No Valida -- le falta el signo para iniciar/usar variables
$myvar= "Valida";
$var7= "Valida";
$_element1= "Valida";
//$house*5 No Valida -- No se pueden hacer operaciones durante el nombramiento de las variables

echo nl2br("\n_myvar $_myvar");
echo nl2br("\n_7var $_7var");
echo nl2br("\nmyvar $myvar");
echo nl2br("\nvar7 $var7");
echo nl2br("\n_element1 $_element1");

echo nl2br("\n\n\n P2 ---- Valores A B C --------------------------------------------------------------");

$a = 'ManejadorSQL';
$b = 'MySQL';
$c = &$a;
echo nl2br("\n P2-a ");
echo nl2br("\n a= $a");
echo nl2br("\n b= $b");
echo nl2br("\n c= $c");

//echo nl2br("\n P2-b ");
unset($a);
unset($b);
$a = "PHP server";
$b = &$a;

echo nl2br("\n P2-c ");
echo nl2br("\n a= $a");
echo nl2br("\n b= $b");
echo nl2br("\n c= $c");

echo nl2br("\n P2-d ");
echo nl2br("\n En el segundo poble de operaciones, primero se libero la memoria de las variables a y b, posteriormete se les asigno nuevos valores, a seria igual a 'PHP server' y b se le asignaria la referencia de a, es decir el contenido de a");

echo nl2br("\n\n\n P3 ---- Impresion de contenidos --------------------------------------------------------------");
unset($a,$b,$c);
$a = "PHP5";  echo nl2br("\n a= $a \nz=");
$z[0] = &$a;   print_r($z);
$b = "5a version de PHP";  echo nl2br("\n b= $b");
$c = intval($b)*10;  echo nl2br("\n c= $c");
$a .= $b;  echo nl2br("\n a= $a");
settype($b,"int");
$b *= $c; echo nl2br("\n b= $b \nz=");
$z[0] = "MySQL";  print_r($z);

echo nl2br("\n\n\n P4 ---- GLOBALS --------------------------------------------------------------------------");
unset($a,$b,$c,$z);

$a = "PHP5";  echo nl2br("\n a= ".$GLOBALS["a"]."\n z=");
$z[] = &$a;   print_r($GLOBALS["z"]);
$b = "5a version de PHP";  echo nl2br("\n b= ".$GLOBALS["b"]."");
$c = intval($b)*10;   echo nl2br("\n c= ".$GLOBALS["c"]."");
$a .= $b;   echo nl2br("\n a= ".$GLOBALS["a"]."");
settype($b,"int");
$b *= $c;   echo nl2br("\n b= ".$GLOBALS["b"]."\nz=");
$z[0] = "MySQL";  print_r($GLOBALS["z"]);

echo nl2br("\n\n P5 ---- SCRIPT --------------------------------------------------------------");
unset($a,$b,$c);

$a = "7 personas";
echo nl2br("\n a=");
var_dump($a);
$b = (integer) $a;
$a = "9E3";
$c = (double) $a;
echo nl2br("\n a=");
var_dump($a);
echo nl2br("\n b=");
var_dump($b);
echo nl2br("\n c=");
var_dump($c);

echo nl2br("\n\n\n P6 ---- var-Dump --------------------------------------------------------------");
unset($a,$b,$c);
$a = "0";
$b = "TRUE";
$c = FALSE;
$d = ($a OR $b);
$e = ($a AND $c);
$f = ($a XOR $b);
echo nl2br("\n a=");
var_dump($a);
echo nl2br("\n b=");
var_dump($b);
echo nl2br("\n c=");
var_dump($c);
echo nl2br("\n d=");
var_dump($d);
echo nl2br("\n e=");
var_dump($e);
echo nl2br("\n f=");
var_dump($f);

echo nl2br("\n c=");
echo json_encode($c);
echo nl2br("\n e=");
echo json_encode($e);

echo nl2br("\n\n\n P7 ---- SERVER --------------------------------------------------------------");
$indicesServer = array('PHP_SELF',
'SERVER_NAME',
'SERVER_SOFTWARE',
'SERVER_PROTOCOL',
'REQUEST_METHOD',
'DOCUMENT_ROOT',
'HTTP_ACCEPT_LANGUAGE',
'SERVER_PORT',
'SERVER_SIGNATURE') ;

echo '<table cellpadding="10">' ;
foreach ($indicesServer as $arg) {
    if (isset($_SERVER[$arg])) {
        echo '<tr><td>'.$arg.'</td><td>' . $_SERVER[$arg] . '</td></tr>' ;
    }
    else {
        echo '<tr><td>'.$arg.'</td><td>-</td></tr>' ;
    }
}
echo '</table>' ;


?>