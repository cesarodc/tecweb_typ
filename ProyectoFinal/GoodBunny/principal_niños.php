<?php
session_start();

if (!isset($_SESSION['usuario'])) {
	echo '
    <script>
        alert("Porfavor inicie sesion");
        window.location = "index.php";
    </script>
    ';
	session_destroy();
	die();
}

include 'php/conexion_be.php';
$id=$_GET['id'];
//echo $id;

$hc = "SELECT * from perfil where id_perfil='$id'";

            $consulta = mysqli_query($conexion, $hc);
            //echo $consulta->num_rows;

            $row = mysqli_fetch_assoc($consulta);

			//print_r($row);
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="assets/css/estilosPrin_2.css">
	<link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Open+Sans:wght@400;600&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat&family=Open+Sans&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.1/normalize.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/glider-js@1.7.3/glider.min.css">
	<title>GoodBunny</title>
	<style>
		.dropdown {
			position: relative;
			display: inline-block;
		}

		.dropdown-content {
			display: none;
			position: absolute;
			background-color: black;
			min-width: 230px;
			box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
			padding: 18px 20px;
			z-index: 1;
		}

		.dropdown:hover .dropdown-content {
			display: block;
		}
	</style>
</head>

<body>
	<header>
		<div class="contenedor">
			<h2 class="logotipo">GoodBunny</h2>
			<nav>
			<a href="#inicio">Inicio</a>
				<a href="#pelis">Películas</a>
				<a href="#series">Series</a>
				<input name="search" id="search" type="search" aria-label="Search" style=" margin-top: 0px; padding: 10px; border: none; background: #494949; font-size: 16px;" placeholder="Buscar...">
				<div class="dropdown">
				<button style="padding: 10px 20px;border: 2px solid #ffb58a;font-size: 18px;background: transparent;font-weight: 600;cursor: pointer;color: white;outline: none;transition: all 300ms;"><img  style="margin-down:0px " src="<?php echo $row["rutaimagen"]; ?>" alt="" width="35px"> <?php echo $row["usuario"]; ?></button>
					<div class="dropdown-content">
						<p> <a href="perfiles.php">Cambiar de perfil</a></p>
						<p><a href="php/cerrar_sesion.php">Cerrar Sesion</a></p>
					</div>
				</div>
			</nav>
		</div>
	</header>

	<div id="result" >
	<div class="indicadores"></div>
			<table id="table" cellspacing="15px">
			<h3 style="color:white;font-family: 'Bebas Neue', cursive; font-weight: normal;font-size:28px; margin-left:20%">Películas Encontradas</h3>
				<thead>
					<tr>
						<td></td>
						<td align="center" style="color:#ffb58a;">Titulo</td>
						<td align="center" style="color:#ffb58a;">Descripción</td>
						<td></td>
					</tr>
				</thead>
				<tbody id="content"></tbody>
			</table>
		</div>
	</div>

	<div id="result1" >
			<div class="indicadores"></div>
			<table id="table1" cellspacing="15px">
			<h3 style="color:white; font-family: 'Bebas Neue', cursive;font-weight: normal; font-size:28px; margin-left:20%">Series Encontradas</h3>
				<thead>
					<tr>
						<td></td>
						<td align="center" style="color:#ffb58a;">Titulo</td>
						<td align="center" style="color:#ffb58a;">Descripción</td>
						<td></td>
					</tr>
				</thead>
				<tbody id="content1"></tbody>
			</table>
		</div>
	</div>

	<main>
		<div class="pelicula-principal">
			<div class="contenedor">
				<h3 class="titulo">Minions:Nace un villano</h3>
				<p class="descripcion">
					En los años 70, Gru crece siendo un gran admirador de "Los salvajes seis", un supergrupo de villanos. Para demostrarles que puede ser malvado, Gru idea un plan con la esperanza de formar parte de la banda. Por suerte, cuenta con la ayuda de sus fieles seguidores, los Minions, siempre dispuestos a sembrar el caos.
				</p>
				<button role="button" class="boton"><i class="fas fa-play"></i>Reproducir</button>
				<button role="button" class="boton"><i class="fas fa-info-circle"></i>Más información</button>
			</div>
		</div>
	</main>

	<div id="inicio" class="peliculas-recomendadas contenedor">
		<div class="contenedor-titulo-controles">
			<h3 style="color:#ffb58a;font-family: 'Bebas Neue', cursive;font-weight: normal;">Películas Y Series Recomendadas</h3>
			<div class="indicadores"></div>
		</div>

		<div class="carousel">
			<div class="carousel__contenedor">
				<button aria-label="Anterior" class="carousel__anterior">
					<i class="fas fa-chevron-left"></i>
				</button>
				<div class="carousel__lista">
					<?php
					include("php/conexion_be.php");

					$pelis = "SELECT * from peliculas WHERE (id_clas='3' OR id_clas='4') AND eliminado = 0";
					$series = "SELECT * from serie WHERE (id_clas='3' OR id_clas='4') AND eliminado = 0";

					$consulta = mysqli_query($conexion, $pelis);
					$consulta2 = mysqli_query($conexion, $series);
					//echo $consulta->num_rows;

					while ($row = mysqli_fetch_assoc($consulta)) { ?>

						<div class="carousel__elemento">
							<img width="100%" height="210" src="<?php echo $row["rutaPortada"]; ?>" alt="">
							<p align="center" style="color:white;"><?php echo $row["titulo"]; ?></p>
							<p align="center" style="color:white;"><?php echo $row["duracion"]; ?></p>
						</div>
					<?php }
					while ($row2 = mysqli_fetch_assoc($consulta2)) { ?>
						<div class="carousel__elemento">
							<img width="100%" height="210" src="<?php echo $row2["rutaPortada"]; ?>" alt="">
							<p align="center" style="color:white;"><?php echo $row2["titulo"]; ?></p>
							<p align="center" style="color:white;"><?php echo $row2["numTemp"]; ?> temporadas</p>
						</div>
					<?php } ?>


				</div>

				<button aria-label="Siguiente" class="carousel__siguiente">
					<i class="fas fa-chevron-right"></i>
				</button>
			</div>

			<div role="tablist" class="carousel__indicadores"></div>
		</div>
	</div>

	<div id="pelis" class="peliculas-recomendadas contenedor">
		<div class="contenedor-titulo-controles">
			<h3 style="color:#ffb58a;font-family: 'Bebas Neue', cursive;font-weight: normal;">Peliculas Animadas</h3>
			<div class="indicadores"></div>
		</div>

		<div class="carousel">
			<div class="carousel__contenedor">
				<button aria-label="Anterior" class="carousel__anterior2">
					<i class="fas fa-chevron-left"></i>
				</button>
				<div class="carousel__lista2">
					<?php
					include("php/conexion_be.php");

					$pelis = "SELECT * from peliculas where id_genero='12' AND (id_clas='3' OR id_clas='4')  AND eliminado = 0";

					$consulta = mysqli_query($conexion, $pelis);
					//$consulta2 = mysqli_query($conexion, $series);
					//echo $consulta->num_rows;

					while ($row = mysqli_fetch_assoc($consulta)) { ?>

						<div class="carousel__elemento">
							<img width="100%" height="210" src="<?php echo $row["rutaPortada"]; ?>" alt="">
							<p align="center" style="color:white;"><?php echo $row["titulo"]; ?></p>
							<p align="center" style="color:white;"><?php echo $row["duracion"]; ?></p>
						</div>
					<?php } ?>


				</div>

				<button aria-label="Siguiente" class="carousel__siguiente2">
					<i class="fas fa-chevron-right"></i>
				</button>
			</div>

			<div role="tablist" class="carousel__indicadores2"></div>
		</div>
	</div>

	<div id="series" class="peliculas-recomendadas contenedor">
		<div class="contenedor-titulo-controles">
			<h3 style="color:#ffb58a;font-family: 'Bebas Neue', cursive;font-weight: normal;">Series de Comedia</h3>
			<div class="indicadores"></div>
		</div>

		<div class="carousel">
			<div class="carousel__contenedor">
				<button aria-label="Anterior" class="carousel__anterior1">
					<i class="fas fa-chevron-left"></i>
				</button>
				<div class="carousel__lista1">
					<?php
					include("php/conexion_be.php");

					
					$series = "SELECT * from serie where  id_genero='4' AND (id_clas='3' OR id_clas='4')  AND eliminado = 0";

					//$consulta = mysqli_query($conexion, $pelis);
					$consulta2 = mysqli_query($conexion, $series);
					//echo $consulta->num_rows;

					while ($row2 = mysqli_fetch_assoc($consulta2)) { ?>
						<div class="carousel__elemento">
							<img width="100%" height="210" src="<?php echo $row2["rutaPortada"]; ?>" alt="">
							<p align="center" style="color:white;"><?php echo $row2["titulo"]; ?></p>
							<p align="center" style="color:white;"><?php echo $row2["numTemp"]; ?> temporadas</p>
						</div>
					<?php } ?>


				</div>

				<button aria-label="Siguiente" class="carousel__siguiente1">
					<i class="fas fa-chevron-right"></i>
				</button>
			</div>

			<div role="tablist" class="carousel__indicadores1"></div>
		</div>
	</div>

	<hr color="white">
	<footer>
		<h2 align="center" style="color:#ffb58a;font-family: 'Bebas Neue', cursive;font-weight: normal;">GoodBunny S.A de C.V</h2>
	</footer>


	<script src="https://kit.fontawesome.com/2c36e9b7b1.js" crossorigin="anonymous"></script>
	<script src="assets/js/main.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/glider-js@1.7.3/glider.min.js"></script>
	<script src="https://kit.fontawesome.com/2c36e9b7b1.js" crossorigin="anonymous"></script>
	<script src="assets/js/app_car.js"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

	<!-- Lógica del Frontend -->
	<script src="search2.js"></script>
</body>

</html>